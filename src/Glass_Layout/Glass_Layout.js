import React, { Component } from 'react'
import './style.css'
import dataGlass from './dataGlasses.json'
export default class Glass_Layout extends Component {
    state = {
        img_url: "./glassesImage/v9.png",
        glassName: "GUCCI G8850U",
        glassPrice: '300',
        glassDesc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
        dataGlass: [
            {
                "id": 1,
                "price": 30,
                "name": "GUCCI G8850U",
                "url": "./glassesImage/v1.png",
                "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
            },
            {
                "id": 2,
                "price": 50,
                "name": "GUCCI G8759H",
                "url": "./glassesImage/v2.png",
                "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
            },
            {
                "id": 3,
                "price": 30,
                "name": "DIOR D6700HQ",
                "url": "./glassesImage/v3.png",
                "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
            },
            {
                "id": 4,
                "price": 70,
                "name": "DIOR D6005U",
                "url": "./glassesImage/v4.png",
                "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
            },
            {
                "id": 5,
                "price": 40,
                "name": "PRADA P8750",
                "url": "./glassesImage/v5.png",
                "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
            },
            {
                "id": 6,
                "price": 60,
                "name": "PRADA P9700",
                "url": "./glassesImage/v6.png",
                "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
            },
            {
                "id": 7,
                "price": 80,
                "name": "FENDI F8750",
                "url": "./glassesImage/v7.png",
                "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
            },
            {
                "id": 8,
                "price": 100,
                "name": "FENDI F8500",
                "url": "./glassesImage/v8.png",
                "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
            },
            {
                "id": 9,
                "price": 60,
                "name": "FENDI F4300",
                "url": "./glassesImage/v9.png",
                "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
            }
        ]
    };

    renderGlassList = () => {
        let glassComponent = this.state.dataGlass.map((item) => {
            return <img className='col-2' key={item.id} src={item.url} alt="" srcset="" onClick={() => {
                this.handleChangeGlassInfo(item.id)
            }} />
        })
        return glassComponent;
    }
    handleChangeGlassInfo = (id) => {
        this.setState({
            img_url: dataGlass[id].url,
            glassName: dataGlass[id].name,
            glassPrice: dataGlass[id].price,
            glassDesc: dataGlass[id].desc,
        })
    }
    render() {

        return (
            <div style={{ backgroundImage: 'url(./glassesImage/background.jpg)', minHeight: '2000px', backgroundSize: '2000px' }}>
                <div style={{ backgroundColor: 'rgba(0,0,0,.8)', minHeight: '2000px' }}>
                    <h3 style={{ backgroundColor: 'rgba(0,0,0,.3)' }} className='text-center text-light p-5'>GLASS APP ONLINE</h3>
                    <div className="container">
                        <div className="row mt-5 text-center">
                            <div className="col-6">
                                <div className="vglasses__card">

                                    <div className="vglasses__model" id="avatar">
                                        <img src={this.state.img_url} alt='' srcSet />
                                    </div>
                                    <div id="glassesInfo" className="vglasses__info" style={{ display: 'block', width: '100%', textAlign: 'left' }}>
                                        <h4>{this.state.glassName}</h4>
                                        <a id="price" className="btn btn-danger" role="button">${this.state.glassPrice}</a>
                                        <p>{this.state.glassDesc} </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-6">
                                <div className="vglasses__card">
                                    <div className="vglasses__model" id="avatar">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row mt-5 text-center">
                            <div className="col-12 vglasses__left">
                                <div className="row">
                                    <div className="col-12">
                                        <h1 className="mb-2">Virtual Glasses</h1>
                                    </div>
                                </div>
                                <div className="row" id="vglassesList">
                                    {this.renderGlassList()}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        )
    }
}
