import logo from './logo.svg';
import './App.css';
import Glass_Layout from './Glass_Layout/Glass_Layout';

function App() {
  return (
    <div className="App">
      <Glass_Layout />
    </div>
  );
}

export default App;
